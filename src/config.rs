use serde::Deserialize;

use crate::loadable::JsonFile;

#[derive(Clone, Debug, Deserialize)]
pub struct DirectoryConfig {
    pub tag: String,
    pub source_path: String,
    pub extracted_path: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct OutputConfig {
    pub chart_directory: String,
    pub copy_charts: bool,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Config {
    pub directories: Vec<DirectoryConfig>,
    pub output: OutputConfig,
}

impl JsonFile for Config {}
