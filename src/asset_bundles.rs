use std::collections::HashMap;

use crate::tsv_file::{TsvFile, TsvLine};

#[derive(Debug, Clone)]
pub struct AssetBundleNamesEntry {
    pub name: String,
    pub abn: String,
}

impl TsvLine for AssetBundleNamesEntry {
    fn from_line(
        line: &str,
        header: &Vec<String>,
        fields: HashMap<String, String>,
    ) -> Result<Self, ()>
    where
        Self: Sized,
    {
        Ok(Self {
            abn: fields.get("abn").cloned().unwrap_or_default(),
            name: fields.get("name").cloned().unwrap_or_default(),
        })
    }
}

#[derive(Debug, Clone)]
pub struct AssetBundleNames {
    pub entries: HashMap<String, String>,
}

impl AssetBundleNames {
    pub fn get_name(&self, abn: &str) -> Option<String> {
        let abn = if abn.ends_with(".bundle") {
            &abn[0..(abn.len() - 7)]
        } else {
            abn
        };
        self.entries.get(abn).cloned()
    }
}

impl TsvFile<AssetBundleNamesEntry> for AssetBundleNames {
    fn from_entries(entries: Vec<AssetBundleNamesEntry>) -> Self {
        let mut map = HashMap::new();
        for entry in entries {
            if entry.abn != "" && entry.name != "" {
                map.insert(entry.abn, entry.name);
            }
        }
        Self { entries: map }
    }
}
