use crate::{asset_bundles::AssetBundleNames, chart_meta::ChartMeta, loadable::JsonFile};
use chrono::DateTime;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct ChartInfo {
    pub filename: String,
    pub tag: String,
    pub md5_hash: String,
    pub abn: String,
    pub name: String,
    pub meta_guid: String,
    pub meta_timestamp: i64,
    pub meta_timestamp_string: String,
}

impl ChartInfo {
    pub fn from_chart_meta(
        chart_meta: ChartMeta,
        filename: String,
        md5_hash: String,
        tag: String,
        asset_bundle_names: &AssetBundleNames,
    ) -> Self {
        let timestamp_seconds = chart_meta.time_created;
        let date = DateTime::from_timestamp(timestamp_seconds, 0).expect("Invalid timestamp");
        let abn = chart_meta.text_script_importer.asset_bundle_name;
        let name = asset_bundle_names
            .get_name(&abn)
            .unwrap_or("<unknown>".to_string());
        ChartInfo {
            name,
            meta_guid: chart_meta.guid,
            meta_timestamp: timestamp_seconds,
            meta_timestamp_string: date.format("%Y-%m-%d %H:%M:%S").to_string(),
            abn,
            md5_hash,
            filename,
            tag,
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize, Default)]
pub struct AssetsDirInfo {
    pub tag: String,
    pub assets_dir: String,
    pub chart_files: Vec<ChartInfo>,
}

#[derive(Debug, Clone, Deserialize, Serialize, Default)]
pub struct AllResults {
    pub directories: Vec<AssetsDirInfo>,
    pub md5_map: HashMap<String, Vec<ChartInfo>>,
    pub abn_map: HashMap<String, Vec<ChartInfo>>,
    pub abn_md5_map: HashMap<String, Vec<String>>,
}

impl JsonFile for AllResults {}

impl AllResults {
    pub fn from_dir_results(dir_results: Vec<AssetsDirInfo>) -> Self {
        let mut md5_map: HashMap<String, Vec<ChartInfo>> = HashMap::new();
        let mut abn_map: HashMap<String, Vec<ChartInfo>> = HashMap::new();
        let mut abn_md5_map: HashMap<String, Vec<String>> = HashMap::new();

        for assets_dir_info in &dir_results {
            for chart_info in &assets_dir_info.chart_files {
                if let Some(vec) = md5_map.get_mut(&chart_info.md5_hash) {
                    vec.push(chart_info.clone())
                } else {
                    md5_map.insert(chart_info.md5_hash.clone(), vec![chart_info.clone()]);
                }

                if let Some(vec) = abn_map.get_mut(&chart_info.abn) {
                    vec.push(chart_info.clone())
                } else {
                    abn_map.insert(chart_info.abn.clone(), vec![chart_info.clone()]);
                }

                if let Some(vec) = abn_md5_map.get_mut(&chart_info.abn) {
                    if !vec.contains(&chart_info.md5_hash) {
                        vec.push(chart_info.md5_hash.clone())
                    }
                } else {
                    abn_md5_map.insert(chart_info.abn.clone(), vec![chart_info.md5_hash.clone()]);
                }
            }
        }
        Self {
            directories: dir_results,
            md5_map,
            abn_map,
            abn_md5_map,
        }
    }
}
