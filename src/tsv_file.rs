use std::{collections::HashMap, fs, io, path::Path};

// TODO: Currently this function does not deal with escaped characters (\t, \n, \r, \\).
// Because of \\, it's not as simple as `x.replace("\\t", "\t")`
//
// The input '\\t' should result in a backslash followed by the letter t
// If you do `x.replace("\\\\", "\\").replace("\\t", "\t")` then input string '\\t' gets interpreted as the tab char
// If you do `x.replace("\\t", "\t").replace("\\\\", "\\")` then input string '\\t' gets interpreted as a backslash followed by a tab char
// (both orderings are incorrect)
//
// A better solution is to loop through the string... or to just ignore the escaped characters for now
fn parse_tsv_line(line: &str) -> Vec<String> {
    line.trim()
        .split("\t")
        .map(|x| x.trim().to_owned())
        .collect()
}

fn get_fields_from_line(line: &str, header: &Vec<String>) -> HashMap<String, String> {
    let segments = parse_tsv_line(line);
    let mut result = HashMap::new();
    for (i, field_name) in header.iter().enumerate() {
        if let Some(value) = segments.get(i) {
            result.insert(field_name.clone(), value.clone());
        }
    }
    return result;
}

pub trait TsvLine {
    fn from_line(
        line: &str,
        header: &Vec<String>,
        fields: HashMap<String, String>,
    ) -> Result<Self, ()>
    where
        Self: Sized;
}

pub trait TsvFile<T>
where
    T: TsvLine,
{
    fn from_entries(entries: Vec<T>) -> Self;

    fn from_file(path: &Path) -> Result<Self, io::Error>
    where
        Self: Sized,
    {
        return Self::from_string(&fs::read_to_string(path)?);
    }

    fn from_string(text: &str) -> Result<Self, io::Error>
    where
        Self: Sized,
    {
        let mut entries = Vec::new();
        let mut header_opt = None;

        // Process all lines
        for line in text.split("\n") {
            // Skip empty lines
            if line.is_empty() {
                continue;
            }

            if let Some(header) = &header_opt {
                // Every line other than the first one is parsed by ABNEntry::from_line
                if let Ok(entry) = T::from_line(line, &header, get_fields_from_line(line, &header))
                {
                    entries.push(entry);
                }
            } else {
                // First line is header
                header_opt = Some(parse_tsv_line(line));
            }
        }

        // Sort entries by number, in case the .tsv data is not sorted yet
        Ok(Self::from_entries(entries))
    }
}
