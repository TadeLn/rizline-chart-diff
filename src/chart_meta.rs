use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::loadable::YamlFile;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct TextScriptImporter {
    #[serde(rename = "externalObjects")]
    pub external_objects: HashMap<(), ()>,

    #[serde(rename = "userData")]
    pub user_data: (),

    #[serde(rename = "assetBundleName")]
    pub asset_bundle_name: String,

    #[serde(rename = "assetBundleVariant")]
    pub asset_bundle_variant: (),
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ChartMeta {
    #[serde(rename = "fileFormatVersion")]
    pub file_format_version: i64,

    pub guid: String,

    #[serde(rename = "timeCreated")]
    pub time_created: i64,

    #[serde(rename = "licenseType")]
    pub license_type: String,

    #[serde(rename = "TextScriptImporter")]
    pub text_script_importer: TextScriptImporter,
}

impl YamlFile for ChartMeta {}
