pub mod asset_bundles;
pub mod chart_meta;
pub mod config;
pub mod loadable;
pub mod processing_results;
pub mod tsv_file;

use std::{
    ffi::OsStr,
    fs,
    path::{Path, PathBuf},
};

use chart_meta::ChartMeta;
use loadable::{JsonFile, LoadError, YamlFile};

use crate::{
    asset_bundles::AssetBundleNames,
    config::Config,
    processing_results::{AllResults, AssetsDirInfo, ChartInfo},
    tsv_file::TsvFile,
};

fn path_to_string(path: &Path) -> String {
    path.to_str()
        .expect("Path cannot be converted to a string")
        .to_string()
}

fn osstr_to_string(os_str: &OsStr) -> String {
    os_str
        .to_str()
        .expect("OsStr cannot be converted to a string")
        .to_string()
}

const ANSI_GO_BACK_A_LINE: &str = "\u{001b}[1F";
const ANSI_ERASE_CURRENT_LINE: &str = "\u{001b}[2K";

#[derive(Debug)]
enum AppError {
    DefaultAssetNotFound,
}

// Try to go into "data/__data/ExportedProject/Assets"
fn find_exported_assets_dir(starting_path: &Path) -> Result<PathBuf, AppError> {
    let mut current_path = starting_path.to_path_buf();

    let segments = ["data", "__data", "ExportedProject", "Assets"];

    // Traverse the directories
    for segment in segments {
        let new_path = current_path.join(segment);
        if new_path.is_dir() {
            current_path = new_path;
        }
    }

    if current_path.join("Default.asset").is_file() {
        return Ok(current_path.to_path_buf());
    } else {
        return Err(AppError::DefaultAssetNotFound);
    }
}

fn find_all_chart_files(assets_dir: &Path) -> Vec<PathBuf> {
    let read_dir = std::fs::read_dir(assets_dir).expect("Cannot read assets dir");
    let mut results = Vec::new();

    for path in read_dir {
        if let Ok(entry) = path {
            let filename = entry
                .file_name()
                .to_str()
                .unwrap_or_default()
                .to_string()
                .to_lowercase();

            if filename.starts_with("chart_") && filename.ends_with(".json") {
                results.push(entry.path());
            }
        }
    }
    results
}

fn process_chart(
    chart_path: &Path,
    tag: &str,
    asset_bundle_names: &AssetBundleNames,
    config: &Config,
) -> Result<ChartInfo, LoadError> {
    let chart_filename = osstr_to_string(
        chart_path
            .file_name()
            .expect("Path needs to have a filename"),
    );

    let chart_meta_path = chart_path
        .parent()
        .expect("Path needs to have a parent dir")
        .join(format!("{chart_filename}.meta"));

    eprintln!("{ANSI_GO_BACK_A_LINE}{ANSI_ERASE_CURRENT_LINE}Processing {chart_filename:#?}");

    let chart_text = std::fs::read_to_string(&chart_path)?;
    let chart_meta = ChartMeta::from_file(&chart_meta_path)?;
    let chart_digest = md5::compute(chart_text);
    let chart_digest_string = format!("{chart_digest:x}",);

    if config.output.copy_charts {
        let output_filename = format!(
            "{}+{}.json",
            if let Some(pretty_name) =
                asset_bundle_names.get_name(&chart_meta.text_script_importer.asset_bundle_name)
            {
                pretty_name
            } else {
                "".to_string()
            },
            chart_digest_string
        );
        let output_path = Path::join(Path::new(&config.output.chart_directory), output_filename);
        fs::copy(chart_path, output_path).expect("Could not copy chart");
    }

    Ok(ChartInfo::from_chart_meta(
        chart_meta,
        chart_filename,
        chart_digest_string,
        tag.to_string(),
        asset_bundle_names,
    ))
}

fn process_all_charts_in_dir(
    assets_dir: &Path,
    tag: &str,
    asset_bundle_names: &AssetBundleNames,
    config: &Config,
) -> Result<AssetsDirInfo, LoadError> {
    // eprintln!("Charts: {charts:#?}, {} items", charts.len());
    eprintln!("Started processing all charts...\n");

    let charts = find_all_chart_files(&assets_dir);

    let mut all_results = AssetsDirInfo::default();
    all_results.tag = tag.to_string();
    all_results.assets_dir = path_to_string(assets_dir);

    for chart_path in charts {
        if let Ok(a) = process_chart(&chart_path, tag, asset_bundle_names, config) {
            all_results.chart_files.push(a);
        } else {
            eprintln!("Processing chart {chart_path:#?} failed\n");
        }
    }

    Ok(all_results)
}

fn process_directory(
    tag: &str,
    source_path: &Path,
    extracted_path: &Path,
    asset_bundle_names: &AssetBundleNames,
    config: &Config,
) -> Result<AssetsDirInfo, LoadError> {
    eprintln!("Processing directory {extracted_path:#?} [{tag}]...");
    let assets_dir = find_exported_assets_dir(extracted_path).expect("Could not find assets dir");
    eprintln!("Found assets dir: {assets_dir:#?}");
    Ok(process_all_charts_in_dir(
        &assets_dir,
        tag,
        asset_bundle_names,
        config,
    )?)
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        let program_name = &args[0];
        eprintln!("Usage: {program_name} <configFile>");
    } else {
        let config_path = &args[1];
        let config = Config::from_file(Path::new(config_path)).expect("Could not load config");

        if !config.output.chart_directory.is_empty() {
            fs::create_dir_all(Path::new(&config.output.chart_directory))
                .expect("Could not create output chart directory");
        }

        let asset_bundle_names = AssetBundleNames::from_file(Path::new("asset_bundles.tsv"))
            .expect("Could not load 'asset_bundles.tsv'");

        let mut all_dir_results = Vec::new();
        for directory in &config.directories {
            let assets_dir_info = process_directory(
                &directory.tag,
                Path::new(&directory.source_path),
                Path::new(&directory.extracted_path),
                &asset_bundle_names,
                &config,
            )
            .unwrap();
            all_dir_results.push(assets_dir_info);
        }

        let all_results = AllResults::from_dir_results(all_dir_results);
        let all_results_json = all_results.to_string(true).unwrap();
        println!("{}", all_results_json);
    }
}
